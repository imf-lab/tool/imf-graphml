<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:g="http://graphml.graphdrawing.org/xmlns"
    xmlns:y="http://www.yworks.com/xml/graphml"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:owl="http://www.w3.org/2002/07/owl#"
    xmlns:skos="http://www.w3.org/2004/02/skos/core#"
    xmlns:f="http://example.org/myFunction#"
    xmlns:imf="http://ns.imfid.org/imf#"
    exclude-result-prefixes="g y f"
    >

  <xsl:variable name="outIRI" select="'http://example.com/id/'"/>
  <xsl:variable name="IMFimportIRI" select="'http://ns.imfid.org/20230630/imf-ontology.owl.ttl'"/>

  <xsl:strip-space elements="*"/>

  <xsl:output method="xml" indent="yes" encoding="UTF-8"/>
  <xsl:namespace-alias stylesheet-prefix="g" result-prefix="#default"/>

  <xsl:function name="f:toID" as="xsd:string">
    <xsl:param name="input" as="xsd:string"/>
    <xsl:sequence select="concat($outIRI, replace($input, ':', '-'))"/>
  </xsl:function>

  <xsl:template match="g:desc|g:key|g:data"/>

  <xsl:template match="g:graph">
    <rdf:RDF>
      <xsl:attribute name="xml:base" select="'http://ns.imfid.org/'"/>

      <owl:Ontology>
        <owl:imports rdf:resource="{$IMFimportIRI}"/>
        <skos:note>Generated <xsl:value-of select="current-dateTime()"/></skos:note>
      </owl:Ontology>
      
      <xsl:apply-templates select="//g:node"/>
      <xsl:apply-templates select="//g:edge"/>

    </rdf:RDF>
  </xsl:template>

  <!-- NODE -->

  <xsl:template match="g:node">
    <rdf:Description>
      <xsl:attribute name="rdf:about"><xsl:value-of select="f:toID(@id)"/></xsl:attribute>
      <xsl:apply-templates select="g:data/y:ShapeNode/*"/>
    </rdf:Description>
  </xsl:template>

  <xsl:template match="y:NodeLabel/text()">
    <skos:prefLabel><xsl:value-of select="normalize-space()"/></skos:prefLabel>
  </xsl:template>

  <!-- element subclasses -->

  <xsl:template match="y:Shape">
    <xsl:choose>
      <xsl:when test="@type = 'rectangle'">
  <rdf:type rdf:resource="imf#Block"/>
      </xsl:when>
      <xsl:when test="@type = 'ellipse'">
  <rdf:type rdf:resource="imf#Connector"/>
      </xsl:when>
      <xsl:when test="@type = 'roundrectangle'">
  <rdf:type rdf:resource="imf#Terminal"/>
      </xsl:when>
      <xsl:otherwise>
  <xsl:variable name="error">Error translating GraphML node to IMF element, unknown node shape: <xsl:value-of select="@type"/>. Expected values: rectangle (Block), ellipse (Connector), roundrectangle (Terminal).</xsl:variable>
  <skos:editorialNote><xsl:value-of select="$error"/></skos:editorialNote>
  <xsl:message><xsl:value-of select="$error"/></xsl:message>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- aspects -->

  <xsl:template match="y:Fill">
    <xsl:choose>
      <xsl:when test="@color = '#FFFF00'">
  <imf:hasAspect rdf:resource="imf#functionAspect"/>
      </xsl:when>

      <xsl:when test="@color = '#00FFFF'">
  <imf:hasAspect rdf:resource="imf#productAspect"/>
      </xsl:when>

      <xsl:when test="@color = '#FF00FF'">
  <imf:hasAspect rdf:resource="imf#locationAspect"/>
      </xsl:when>

      <!-- null aspect -->
      <xsl:when test="@color = '#FFFFFF'">
      </xsl:when>

      <!-- unknown aspect -->
      <xsl:when test="@color = '#CCCCCC'">
      </xsl:when>

      <xsl:otherwise>
  <xsl:variable name="error">Error translating GraphML node to IMF element, unknown node fill color: <xsl:value-of select="@color"/>. Expected values: #FFFF00 (Function), #00FFFF (Product), #FF00FF (Location), #FFFFFF (No aspect), #CCCCCC (Unknown aspect).</xsl:variable>
  <skos:editorialNote><xsl:value-of select="$error"/></skos:editorialNote>
  <xsl:message><xsl:value-of select="$error"/></xsl:message>
      </xsl:otherwise>

    </xsl:choose>
  </xsl:template>


  <!-- EDGE -->

  <xsl:template match="g:edge">

    <xsl:variable name="edgeID" select="@id"/>
    <xsl:variable name="edgeSource" select="@source"/>
    <xsl:variable name="edgeTarget" select="@target"/>

    <!-- TODO will this work for other kinds of edges? -->
    <xsl:for-each select="g:data/y:PolyLineEdge">

      <xsl:variable name="arrowSource" select="y:Arrows/@source"/>
      <xsl:variable name="arrowTarget" select="y:Arrows/@target"/>
      <xsl:variable name="lineType" select="y:LineStyle/@type"/>
      <xsl:variable name="lineWidth" select="y:LineStyle/@width"/>

      <xsl:choose>

  <!-- sameAs -->
  <xsl:when test="$arrowSource = 'none' and $arrowTarget = 'none' and $lineType = 'line' and $lineWidth = '5.0'">
    <rdf:Description rdf:about="{f:toID($edgeSource)}">
      <owl:sameAs rdf:resource="{f:toID($edgeTarget)}"/>
    </rdf:Description>
  </xsl:when>

  <!-- connectedTo -->
  <xsl:when test="$arrowSource = 'none' and $arrowTarget = 'none' and $lineType = 'line'">
    <rdf:Description rdf:about="{f:toID($edgeSource)}">
      <imf:connectedTo rdf:resource="{f:toID($edgeTarget)}"/>
    </rdf:Description>
  </xsl:when>
  
  <!-- transfersTo -->
  <xsl:when test="$arrowSource = 'none' and $arrowTarget = 'delta' and $lineType = 'line'">
    <rdf:Description rdf:about="{f:toID($edgeSource)}">
      <imf:transfersTo rdf:resource="{f:toID($edgeTarget)}"/>
    </rdf:Description>
  </xsl:when>
  <xsl:when test="$arrowSource = 'delta' and $arrowTarget = 'none' and $lineType = 'line'">
    <rdf:Description rdf:about="{f:toID($edgeTarget)}">
      <imf:transfersTo rdf:resource="{f:toID($edgeSource)}"/>
    </rdf:Description>
  </xsl:when>

  
  <!-- partOf + hasPart -->
  <xsl:when test="$arrowSource = 'none' and $arrowTarget = 'diamond' and $lineType = 'line'">
    <rdf:Description rdf:about="{f:toID($edgeSource)}">
      <imf:partOf rdf:resource="{f:toID($edgeTarget)}"/>
    </rdf:Description>
  </xsl:when>
  <xsl:when test="$arrowSource = 'diamond' and $arrowTarget = 'none' and $lineType = 'line'">
    <rdf:Description rdf:about="{f:toID($edgeSource)}">
      <imf:hasPart rdf:resource="{f:toID($edgeTarget)}"/>
    </rdf:Description>
  </xsl:when>

  <!-- specialisationOf -->
  <xsl:when test="$arrowSource = 'none' and $arrowTarget = 'white_delta' and $lineType = 'line'">
    <rdf:Description rdf:about="{f:toID($edgeSource)}">
      <imf:specialisationOf rdf:resource="{f:toID($edgeTarget)}"/>
    </rdf:Description>
  </xsl:when>
  <xsl:when test="$arrowSource = 'white_delta' and $arrowTarget = 'none' and $lineType = 'line'">
    <rdf:Description rdf:about="{f:toID($edgeTarget)}">
      <imf:specialisationOf rdf:resource="{f:toID($edgeSource)}"/>
    </rdf:Description>
  </xsl:when>

  <!-- fulfilledBy, Note that we "reverse" the arrow to fulfilledBy. -->
  <xsl:when test="$arrowSource = 'white_delta' and $arrowTarget = 'none' and $lineType = 'dashed'">
    <rdf:Description rdf:about="{f:toID($edgeSource)}">
      <imf:fulfilledBy rdf:resource="{f:toID($edgeTarget)}"/>
    </rdf:Description>
  </xsl:when>
  <xsl:when test="$arrowSource = 'none' and $arrowTarget = 'white_delta' and $lineType = 'dashed'">
    <rdf:Description rdf:about="{f:toID($edgeTarget)}">
      <imf:fulfilledBy rdf:resource="{f:toID($edgeSource)}"/>
    </rdf:Description>
  </xsl:when>

  
  <!-- proxy -->
  <xsl:when test="$arrowSource = 'none' and $arrowTarget = 'none' and $lineType = 'dotted'">
    <rdf:Description rdf:about="{f:toID($edgeSource)}">
      <imf:proxy rdf:resource="{f:toID($edgeTarget)}"/>
    </rdf:Description>
  </xsl:when>

  <!-- projection -->
  <xsl:when test="$arrowSource = 'none' and $arrowTarget = 'delta' and $lineType = 'dotted'">
    <rdf:Description rdf:about="{f:toID($edgeSource)}">
      <imf:projection rdf:resource="{f:toID($edgeTarget)}"/>
    </rdf:Description>
  </xsl:when>
  <xsl:when test="$arrowSource = 'delta' and $arrowTarget = 'none' and $lineType = 'dotted'">
    <rdf:Description rdf:about="{f:toID($edgeTarget)}">
      <imf:projection rdf:resource="{f:toID($edgeSource)}"/>
    </rdf:Description>
  </xsl:when>


  <!-- error if no match -->
  <xsl:otherwise>
    <xsl:variable name="error">Error translating GraphML edge <xsl:value-of select="$edgeID"/> to IMF relation, unknown edge attribute combination: Arrows/@source=<xsl:value-of select="$arrowSource"/>, Arrows/@target=<xsl:value-of select="$arrowTarget"/>, LineStyle/@type=<xsl:value-of select="$lineType"/></xsl:variable>
    <rdf:Description>
      <xsl:attribute name="rdf:nodeID"><xsl:value-of select="f:toID($arrowSource)"/></xsl:attribute>
      <skos:editorialNote><xsl:value-of select="$error"/></skos:editorialNote>
    </rdf:Description>
    <xsl:message><xsl:value-of select="$error"/></xsl:message>
  </xsl:otherwise>

      </xsl:choose>
    </xsl:for-each>

  </xsl:template>

</xsl:stylesheet>
