.PHONY: all test install
.PRECIOUS: example/%.rdf

# DATA
data-input := $(wildcard example/*.graphml)
data-output = $(data-input:.graphml=.ttl)

# SOFTWARE
JENA-version = apache-jena-4.10.0
JENA = bin/$(JENA-version)

# note that the download of Saxon has a different name
SAXON-version = saxon-he-10.5.jar
SAXON = bin/saxon/$(SAXON-version)
run-SAXON = java -jar bin/saxon/$(SAXON-version)

$(SAXON):
	mkdir -p $(dir $(SAXON))
	wget -O $(SAXON-version:.jar=.zip) https://sourceforge.net/projects/saxon/files/Saxon-HE/10/Java/SaxonHE10-5J.zip/download
	unzip -u $(SAXON-version:.jar=.zip) -d $(dir $(SAXON))

$(JENA):
	mkdir -p bin
	wget https://archive.apache.org/dist/jena/binaries/$(JENA-version).zip -O $@.zip
	unzip -u $@.zip -d bin

install: \
	$(SAXON) \
	$(JENA)

example/%.rdf: example/%.graphml graphml2rdf.xsl
	$(run-SAXON) -o:$@ -xsl:graphml2rdf.xsl $<

example/%.ttl: example/%.rdf
	$(JENA)/bin/riot --output N3 $^ > $@

all: $(data-output)

clean:
	rm -f $(data-output)
	rm -fR bin
